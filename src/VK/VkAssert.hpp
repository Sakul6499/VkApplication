#ifndef TESTING_VKASSERT_HPP
#define TESTING_VKASSERT_HPP

#include <cstdlib>

#define ASSERT_VK(val)\
    if(val != VK_SUCCESS){\
            std::cout << "!!! VK Assert failed !!!"; \
            std::cerr << "!!! VK Assert failed !!!"; \
            abort();\
    }

#endif //TESTING_VKASSERT_HPP
