#ifndef TESTING_SPLITPATH_HPP
#define TESTING_SPLITPATH_HPP

#include <vector>
#include <string>
#include <set>

std::vector<std::string> splitPath(std::string &str, std::set<char> delimiters);

#endif //TESTING_SPLITPATH_HPP
